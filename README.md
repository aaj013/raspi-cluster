# Inventory
- Raspberry Pi 1 - 10
- Adapter - 3 (More rqd)
- SD Card - 5 (not rqd if pxe set)
- Switch - 1
- LAN cable - 3 (more rqd)
- VGA HDMI convertor - 1
- VGA cable - from sampath sir room
- Monitor - vaisakhs
- Keyboard - seema
- extension board - rqd

# Steps

## Nodes
  | hostname | IP addr       | Version|
  |----------|---------------|--------|
  | node1    | 192.168.64.13 |        |
  | node2    | 192.168.64.17 |        |
  | node3    | 192.168.64.19 |        |

## Commands
 - Shutdown
   - ansible <node1> --sudo -m raw -a "sudo shutdown -h now"

## Useful links
- https://www.youtube.com/watch?v=vflPGqcXJkY
- [Raspbian Stretch Lite](https://downloads.raspberrypi.org/raspbian_lite_latest)
- Resolve remote libvirt issue (no polkit agent) https://www.cnblogs.com/EasonJim/p/9596012.html